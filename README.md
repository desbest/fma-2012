# FMA 2012 Dokuwiki Theme

Dokuwiki theme for version Adora Belle 2012, and is online for future reference reasons.
This theme is not compatible with Dokuwiki version Hrun 2014 and Greebo 2018 or later, refer for the FMA 2014 or 2018 theme for that.

This theme is based on the default template for DokuWiki that was used until 2012.
It was extracted from the core late 2013 and since then not actively maintained by the core developers anymore.

[theme made by desbest](http://desbest.com)

![fma 2012 theme for dokuwiki screenshot](https://i.imgur.com/VtZPkba.png)

![example website using fma theme](https://i.imgur.com/JcIxd8n.png)